﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481HW3App1
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class dogPage
        : ContentPage
	{
		public dogPage (string passData)
		{
			InitializeComponent ();
            passed.Text = passData;
        
		}
	}
}